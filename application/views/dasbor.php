<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN AKADIN.ID</title>
    <?php $this->load->view('parts/style'); ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <?php 
        #$this->load->view('parts/navbar'); 
        $this->load->view('parts/sidebar');
        $this->load->view('parts/alerts');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top: 0 !important">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-5">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Admin Akadin.ID </h1>
                </div><!-- /.col -->
                <div class="col-sm-6 content-desktop">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row content-desktop">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="float-left">List Undangan</h5>
                                <a href="<?= base_url('invitation/add?r=homepage') ?>" class="btn btn-primary float-right">Tambah</a>
                            </div>
                            <div class="card-body">
                                <table class="table data-table">
                                    <thead>
                                        <th>#</th>
                                        <th>Tanggal dibuat</th>
                                        <th>Judul Undangan</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($invitation->result() as $i => $inv): ?>
                                        <tr>
                                            <td><?= $i+1 ?></td>
                                            <td><?= date('d M Y, h:i',$inv->date_created) ?></td>
                                            <td><a href="<?= base_url('e/'.$inv->slug) ?>" target="_blank" /><?= $inv->name ?> <small><i class="fas fa-external-link-alt"></i></small></a></td>
                                            <td><?= $inv->slug ?></td>
                                            <td>
                                                <form action="<?= base_url('invitation/list') ?>" id="formChangeStatus<?= $i ?>" method="POST">
                                                <input type="hidden" name="id" value="<?= $inv->id ?>" />
                                                <input type="hidden" name="r" value="1">
                                                <input type="hidden" name="status" value="<?= $inv->status ?>" />
                                                <div class="switch-field">
                                                    <input type="radio" class="radio-on" id="radio-one<?= $i ?>" name="switch-one" value="active" <?php if($inv->status == 1) { echo "checked"; } ?>/>
                                                    <label class="on" for="radio-one<?= $i ?>">ON</label>
                                                    <input type="radio" class="radio-off" id="radio-two<?= $i ?>" name="switch-one" value="off" <?php if($inv->status == 0) { echo "checked"; } ?>/>
                                                    <label class="off" for="radio-two<?= $i ?>">OFF</label>
                                                </div>
                                                </form>
                                            </td>
                                            <td>
                                                <a href="<?= base_url('invitation/'.$inv->id.'/edit') ?>" class="btn btn-warning btn-sm">edit</a>
                                                <a href="<?= base_url('invitation/'.$inv->id.'/del') ?>" class="btn btn-danger btn-sm" onclick="return del_confirm()">hapus</a>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-body content-mobile">
                    <div class="table-responsive ">
                        <table class="table data-table">
                            <thead>
                                <th>#</th>
                                <th>Konten</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                            <?php foreach($invitation->result() as $i => $inv): ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td>
                                        <a href="<?= base_url('e/'.$inv->slug) ?>" target="_blank" /><?= $inv->name ?> <small><i class="fas fa-external-link-alt"></i></small></a>
                                        <small><?= date('d M Y, h:i',$inv->date_created) ?></small>
                                    <td>
                                        <form action="<?= base_url('invitation/list') ?>" id="formChangeStatus<?= $i ?>" method="POST">
                                        <input type="hidden" name="id" value="<?= $inv->id ?>" />
                                        <input type="hidden" name="r" value="1">
                                        <input type="hidden" name="status" value="<?= $inv->status ?>" />
                                        <div class="switch-field">
                                            <input type="radio" class="radio-on" id="radio-one<?= $i ?>" name="switch-one" value="active" <?php if($inv->status == 1) { echo "checked"; } ?>/>
                                            <label class="on" for="radio-one<?= $i ?>">ON</label>
                                            <input type="radio" class="radio-off" id="radio-two<?= $i ?>" name="switch-one" value="off" <?php if($inv->status == 0) { echo "checked"; } ?>/>
                                            <label class="off" for="radio-two<?= $i ?>">OFF</label>
                                        </div>
                                        </form>
                                        <a href="<?= base_url('invitation/'.$inv->id.'/edit') ?>" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
                                        <a href="<?= base_url('invitation/'.$inv->id.'/del') ?>" class="btn btn-danger btn-sm" onclick="return del_confirm()"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <?php foreach($template as $i => $t): ?>
                    <div class="col-md-3 col-6">
                        <div class="card">
                            <img src="<?= $t->ui_desktop ?>" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><?= $t->filename ?></h5>
                                <br><hr>
                                <a href="<?= base_url('example/'.$t->filename) ?>" target="blank" class="btn btn-primary btn-sm">Preview</a>
                                <a href="<?= base_url('invitation/add/?t='.$t->filename.'&r=homepage') ?>" class="btn btn-primary btn-sm">Buat</a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> akadin.ID</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
        Powered by <b>Gidicode Project</b>
        </div>
    </footer>
</div>
<!-- ./wrapper -->

<?php $this->load->view('parts/script') ?>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/admin/') ?>plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="<?= base_url('assets/admin/') ?>js/pages/dashboard2.js"></script>

<?php foreach($invitation->result() as $a => $ins): ?>
<script>
    $(document).ready(function(){
        $("#radio-one<?= $a ?>").click(function(){        
            $("#formChangeStatus<?= $a ?>").submit();
        });

        $("#radio-two<?= $a ?>").click(function(){        
            $("#formChangeStatus<?= $a ?>").submit();
        });
    });
</script>
<?php endforeach; ?>

</body>
</html>
