<?php if($this->session->flashdata('info')){ ?>
    <div class="alert alert-info alert-dismissible fade show alert-fixed" id="alert-dismiss" role="alert">
        <strong><?= $this->session->flashdata('info') ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success alert-dismissible fade show alert-fixed" id="alert-dismiss" role="alert">
        <strong><?= $this->session->flashdata('success') ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>

<?php if($this->session->flashdata('danger')){ ?>
    <div class="alert alert-danger alert-dismissible fade show alert-fixed" id="alert-dismiss" role="alert">
        <strong><?= $this->session->flashdata('danger') ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php } ?>