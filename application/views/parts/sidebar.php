<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    
  </nav>
  <!-- /.navbar -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url() ?>" class="brand-link">
    <span class="brand-text font-weight-light">Admin Akadin.ID </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <a href="#" class="d-block">Hai <?= $this->session->userdata('username') ?></a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="<?= base_url() ?>" class="nav-link <?php if(!$this->uri->segment(2)) { echo "active"; }  ?>">
            <i class="far fa-circle nav-icon"></i>
            <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-header">e-Invitations</li>
        <li class="nav-item">
            <a href="<?= base_url('invitation/add') ?>" class="nav-link 
                <?php if($this->uri->segment(1) == 'invitation' && $this->uri->segment(2) == 'add') { echo "active"; }  ?>">
            <i class="nav-icon fas fa-plus"></i>
            <p>
                Tambah
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?= base_url('invitation/list') ?>" class="nav-link <?php if($this->uri->segment(1) == 'invitation' && $this->uri->segment(2) == 'list') { echo "active"; }  ?>" />
            <i class="nav-icon far fa-envelope"></i>
            <p>
                List Undangan
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?= base_url('invitation/template') ?>"" class="nav-link <?php if($this->uri->segment(1) == 'invitation' && $this->uri->segment(2) == 'template') { echo "active"; }  ?>">
            <i class="nav-icon fas fa-images"></i>
            <p>
                List Template
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?= base_url('invitation/music') ?>"" class="nav-link <?php if($this->uri->segment(1) == 'invitation' && $this->uri->segment(2) == 'music') { echo "active"; }  ?>">
            <i class="nav-icon fas fa-music"></i>
            <p>
                List Musik
            </p>
            </a>
        </li>
        <li class="nav-header">MANAJEMEN PROFIL</li>
        <li class="nav-item">
            <a href="<?= base_url('dasbor/profile') ?>" class="nav-link <?php if($this->uri->segment(2) == 'profile') { echo "active"; }  ?>">
            <i class="nav-icon fas fa-user"></i>
            <p>
                Edit Profil dan Password
            </p>
            </a>
        </li>
        <li class="nav-header">-</li>
        <li class="nav-item">
            <a href="<?= base_url('auth/logout') ?>" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <p>
                Keluar
            </p>
            </a>
        </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>