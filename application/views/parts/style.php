<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url('assets/admin/') ?>plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="<?= base_url('assets/admin/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url('assets/admin/') ?>css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- DataTable -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

<style>
    .error {
        color: red !important;
        font-size: 11pt;
    }

    .elevation-4 {
        box-shadow: none !important;
    }

    .switch-field {
        display: flex;
        margin-bottom: 36px;
        overflow: hidden;
    }

    .switch-field input {
        position: absolute !important;
        clip: rect(0, 0, 0, 0);
        height: 1px;
        width: 1px;
        border: 0;
        overflow: hidden;
    }

    .switch-field label {
        background-color: #e4e4e4;
        color: rgba(0, 0, 0, 0.6);
        font-size: 14px;
        line-height: 1;
        text-align: center;
        padding: 8px 16px;
        margin-right: -1px;
        border: 1px solid rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
        transition: all 0.1s ease-in-out;
    }

    .switch-field label:hover {
        cursor: pointer;
    }

    .switch-field input:checked + label.on {
        background-color: #a5dc86;
        box-shadow: none;
    }

    .switch-field input:checked + label.off {
        background-color: #ff2212;
        box-shadow: none;
    }

    .switch-field label:first-of-type {
        border-radius: 4px 0 0 4px;
    }

    .switch-field label:last-of-type {
        border-radius: 0 4px 4px 0;
    }

    .alert-fixed {
        position:fixed;
        top: 10px;
        right: 5px;
    }

    .media-pic {
        margin-bottom: 10px;
    }

    .media-pic a {
        position: absolute;
        width: 30px;
        height: 30px;
        background-color: #dc3545!important;
        padding: 5px 7px 9px 9px;
        font-size: 10pt;
        border-radius: 100%;
        color: #fff !important;
        right: 0;
        top: auto;
        margin-top:-10px;
        cursor:pointer;
    }

    .content-mobile {
        display: none;
    }

    @media (max-width: 480px) {
        .content-desktop {
            display: none;
        }

        .content-mobile {
            display: block;
        }
        .dataTables_length {
            display:none !important;
        }
        .switch-field label {
            font-size: 8pt;
            padding: 4px 7px;
        }

        .btn-group-sm>.btn, .btn-sm {
            font-size: 8pt !important;
        }

        .switch-field {
            margin-bottom: 0;
        }
    }
</style>