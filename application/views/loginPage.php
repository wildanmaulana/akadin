<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LOGIN | ADMIN AKADIN.ID</title>
    <?php $this->load->view('parts/style'); ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b>Admin</b> Akadin.ID</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <?php if($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger">
                <?= $this->session->flashdata('error') ?>
            </div>
            <?php } ?>
            <form action="<?= base_url('auth') ?>" method="post">
            <div class="input-group mb-3">
                <input type="text" name="username" class="form-control" placeholder="username">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
            <!-- /.col -->
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat w-100">Sign In</button>
            </div>
            <!-- /.col -->
            </div>
        </div>
    </div>
    </form>

    </div>
</div>
<!-- /.login-box -->

<?php $this->load->view('parts/script'); ?>

</body>
</html>
