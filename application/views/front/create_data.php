<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Zuwaja - Undangan Nikah dan Sumbangan Nikah Online</title>
        <?php $this->load->view('front/parts/head') ?>
    </head>
    <body>
        
        <?php $this->load->view('front/parts/header') ?>

        <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="ftco-animate mt-5">
                        <h3 class="text-white">Buat Undangan</h3>
                        <p class="breadcrumbs mb-0">
                            <span class="mr-3"><a href="<?= base_url('create') ?>">Pilih tema <i class="ion-ios-arrow-forward"></i></a></span> 
                            <span class="mr-3"><a href="<?= base_url('create/addon') ?>">Pilih paket tambahan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3"><a href="<?= base_url('create/data') ?>">Edit Undangan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3">Pembayaran <i class="ion-ios-arrow-forward"></i></span>
                            <span>Undangan Terbit</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-section bg-light pb-5 pt-5">
            <div class="container ftco-animate">
                <form action="#" method="get">
                <div class="row">
                    <div class="col-md-9 mx-auto">
                        <div id="accordion" class="inv-form">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="float-left pt-3 pl-3 pb-0">Data Pemesan</h5>
                                    <a class="float-right text-primary edit" id="f1"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Edit
                                    </a>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="">Nama Lengkap</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Email</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">No HP</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <a href="#" class="btn btn-primary float-right mb-4" id="s1" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Lanjutkan</a>
                                </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="float-left pt-3 pl-3 pb-0">Konten Undangan</h5>
                                    <a class="float-right text-primary d-none edit" id="f2"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Edit
                                    </a>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <?php 
                                        $data['w'] = $wedding;
                                        $this->load->view('form_undangan',$data); ?>
                                        <br>
                                        <a href="#" class="btn btn-primary float-right mb-4" id="s2">Lanjutkan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </section>

        <?php $this->load->view('front/parts/footer'); ?>
        <?php $this->load->view('front/parts/script'); ?>
        <script>
            $( document ).ready(function() {
                if($("#collapseOne").hasClass('show') == true) {
                    $("#f1").addClass('d-none');
                }

                if($("#s1")).click(function(){
                    $("#f1").removeClass('d-none');
                });
            });

            
            $( "#withAkad" ).on( "click", function() {
        $("#infoAkad").css('display','block');
    });

    $( "#noAkad" ).on( "click", function() {
        $("#infoAkad").css('display','none');
    });

    $( "#withGaleri" ).on( "click", function() {
        $("#addgaleri").css('display','block');
    });

    $( "#noGaleri" ).on( "click", function() {
        $("#addgaleri").css('display','none');
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('#galeri').fileuploader({
        addMore: true
    });

    $(document).ready(function () {
        jQuery.validator.addMethod("usrregex", function(value, element){
            if (/[^a-zA-Z0-9]/.test(value)) {
                return false;
            } else {
                return true;
            };
        }, "Oops, Gunakan karakter yang benar.. "); 

        $("#formAdd").validate({
            rules: {
                url: {
                    required: true,
                    minlength: 3,
                    maxlength: 15,
                    usrregex: true,
                    remote: {
                        url: "<?php echo base_url('dasbor/cekslug') ?>",
                        type: "post",
                        data: {
                            slug: function() {
                                return $( "#url" ).val();
                            }
                        }
                    }
                }
            },
            messages: {
                url: {
                    remote: "Oops, URL sudah terpakai",
                    regex: "Oops, harap menggunakan karakter yang benar"
                }
            }
        });
    });
        </script>
    </body>
</html>