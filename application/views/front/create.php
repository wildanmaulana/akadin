<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Zuwaja - Undangan Nikah dan Sumbangan Nikah Online</title>
        <?php $this->load->view('front/parts/head') ?>
    </head>
    <body>
        
        <?php $this->load->view('front/parts/header') ?>

        <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="ftco-animate mt-5">
                        <h3 class="text-white">Buat Undangan</h3>
                        <p class="breadcrumbs mb-0">
                            <span class="mr-3"><a href="<?= base_url('create') ?>">Pilih tema <i class="ion-ios-arrow-forward"></i></a></span> 
                            <span class="mr-3">Pilih paket tambahan <i class="ion-ios-arrow-forward"></i></span>
                            <span class="mr-3">Edit Undangan <i class="ion-ios-arrow-forward"></i></span>
                            <span class="mr-3">Pembayaran <i class="ion-ios-arrow-forward"></i></span>
                            <span>Undangan Terbit</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-section services-section bg-light ftco-no-pb">
            <div class="container">
                <div class="row">
                    <?php foreach($invitation->result() as $inv): ?>
                    <div class="col-md-4 col-6 ftco-animate">
                        <div class="blog-entry">
                            <a href="#" class="block-20" style="background-image: url('<?= $inv->ui_desktop; ?>');"></a>
                            
                            <div class="text" style="padding-top: 1rem !important">
                                <div class="meta mb-2">
                                <small>Harga mulai dari <strong class="text-primary">Rp. <?= number_format($inv->price) ?></strong></small>
                                </div>
                            </div>
                            <div class="text d-flex" style="padding-top: 0rem !important">
                                <div class="desc">
                                    <h3 class="heading"><a href="#">Template <?= $inv->filename ?></a></h3>
                                </div>
                                <div class="btn-inv text-right" style="margin-left: auto; order: 2;">
                                    <a href="#" class="btn btn-sm btn-outline-warning" target="_blank">Lihat Demo</a>
                                    <a href="#" class="btn btn-sm btn-primary" target="_blank">Pesan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <?php $this->load->view('front/parts/footer'); ?>
        <?php $this->load->view('front/parts/script'); ?>
    </body>
</html>