<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">

<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/open-iconic-bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/animate.css">

<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/magnific-popup.css">

<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/aos.css">

<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/ionicons.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/jquery.timepicker.css">


<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/flaticon.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/icomoon.css">
<link rel="stylesheet" href="<?= base_url('assets/front/') ?>css/style.css">