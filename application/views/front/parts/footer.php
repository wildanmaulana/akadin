<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2 mb-2">Zuwaja</h2>
                    <p>Platform untuk membuat undangan nikah online dan juga sumbangan nikah online</p>
                    <br>
                    <h2 class="ftco-heading-2 mb-1">Metode Pembayaran</h2>
                    <p style="font-size:9pt; line-height:1.5">Payment gateway supported by Xendit and Midtrans. Engine supported by Gidicode Gateway</p>
                    <img src="<?= base_url('assets/front/images/bni_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/mandiri_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/bri_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/permatabank.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/atmbersama_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/gopay_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/ovo_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/mastercard_white.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/visa.jpg') ?>" alt="" style="height:40px">
                    <img src="<?= base_url('assets/front/images/jcb_white.jpg') ?>" alt="" style="height:40px">
                </div>
            </div>

            <div class="col-md-8">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2 mb-2">Tentang Kami</h2>
                    <p>Zuwaja merupakan produk yang dikembangkan oleh <a href="https://gidicode.com" target="_blank">Gidicode Project</a></p>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Innovation Factory Block71 Yogyakarta, Jln Prof. Herman Yohanes No.1212, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223 <br> Indonesia</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">+62 813 36723747</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">zuwaja@gidicode.com</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
    </div>
</footer>