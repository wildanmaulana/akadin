<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Zuwaja - Undangan Nikah dan Sumbangan Nikah Online</title>
        <?php $this->load->view('front/parts/head') ?>
    </head>
    <body>
        
        <?php $this->load->view('front/parts/header') ?>

        <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="ftco-animate mt-5">
                        <h3 class="text-white">Buat Undangan</h3>
                        <p class="breadcrumbs mb-0">
                            <span class="mr-3"><a href="<?= base_url('create') ?>">Pilih tema <i class="ion-ios-arrow-forward"></i></a></span> 
                            <span class="mr-3"><a href="<?= base_url('create/addon') ?>">Pilih paket tambahan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3">Edit Undangan <i class="ion-ios-arrow-forward"></i></span>
                            <span class="mr-3">Pembayaran <i class="ion-ios-arrow-forward"></i></span>
                            <span>Undangan Terbit</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-section bg-light pb-5 pt-5">
            <div class="container ftco-animate">
                <form action="#" method="get">
                <div class="row">
                    <div class="col-md-4 col-12 mb-5">
                        <div class="d-flex">
                            <input type="hidden" name="template_val" id="template_val" value="50000" />
                            <img src="<?= base_url('assets/uploads/A11.jpg') ?>" class="mr-3" alt="" style="width: 120px">
                            <div class="text">
                            <h6>Template A3</h6>
                            <p>Rp. 50,000</p>
                            <a href="#">Ganti template</a>
                            </div>
                        </div>
                        <hr>
                        <div class="d-none d-sm-block">
                            <h5>Total Harga</h5>
                            <table class="table table-sm text-left">
                                <tbody>
                                    <tr>
                                        <td>Template</td>
                                        <td id="template"></td>
                                    </tr>
                                    <tr>
                                        <input type="hidden" id="ytb1" value="0">
                                        <input type="hidden" id="comm1" value="0">
                                        <td>Addon</td>
                                        <td id="addon"></td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td id="total"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <h5>Addon</h5>
                        <input type="hidden" name="template" value="a1">
                        <input type="hidden" value="50000" id="ytb_val">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ytb" name="ytb" value="1">
                            <label class="custom-control-label" for="ytb" style="line-height:1.2">
                                <strong>Video Youtube dan iringan musik</strong> <br>
                                <small>Rp. 50,000</small> <br>
                                Tambahkan video youtube dan iringan musik pada undangan anda, sehingga undangan pernikahan anda terasa lebih meriah
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="comm_val" value="75000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="comm" name="comm" value="1">
                            <label class="custom-control-label" for="comm" style="line-height:1.2">
                                <strong>Komentar dan Konfirmasi Kehadiran</strong> <br>
                                <small>Rp. 75,000</small> <br>
                                Tambahkan fitur ini jika anda ingin mendapatkan data kehadiran dan komentar untuk kesan dan pesan pernikahan anda
                            </label>
                        </div>
                        <hr>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="smb" name="smb" value="1">
                            <label class="custom-control-label" for="smb" style="line-height:1.2">
                                <strong>Sumbangan Online</strong> <br>
                                <small>Rp. 0</small> <br>
                                Tambahkan fitur ini jika anda ingin menerima sumbangan online. Perlu diketahui, <strong>anda akan dikenakan biaya 5% dari total sumbangan yang terkumpul untuk anda</strong>. <br><small>Simulasi: Jika anda menerima sumbangan sebesar Rp. 1,000,000 maka akan kami potong sebesar 5% atau Rp. 50,000 untuk biaya Payment Gateway</small>
                            </label>
                        </div>
                        <div class="mt-5"></div>
                        <button type="submit" class="btn btn-primary float-right">Lanjutkan</button>
                    </div>
                </div>
                </form>
            </div>
        </section>

        <?php $this->load->view('front/parts/footer'); ?>
        <?php $this->load->view('front/parts/script'); ?>
        <script>
            $( document ).ready(function() {
                var ytb = 0;
                var comm = 0;
                var template = parseInt($("#template_val").val());
                $("#template").html("Rp. "+template);

                $("#ytb").change(function() {
                    
                    if ($("#ytb").is(':checked')) {
                        $("#ytb1").val(parseInt($("#ytb_val").val()));
                        
                    } else {
                        $("#ytb1").val(0);
                    }
                    var total_addon = parseInt($("#ytb1").val()) + parseInt($("#comm1").val());
                    var total = total_addon + template;
                    $("#addon").text("Rp. "+total_addon);
                    $("#total").text("Rp. "+total);
                });

                $("#comm").change(function() {
                    if ($("#comm").is(':checked')) {
                        $("#comm1").val(parseInt($("#comm_val").val()));
                    } else {
                        $("#comm1").val(0);
                    }

                    var total_addon = parseInt($("#ytb1").val()) + parseInt($("#comm1").val());
                    var total = total_addon + template;
                    $("#addon").text("Rp. "+total_addon);
                    $("#total").text("Rp. "+total);
                });
                var total = template;
                $("#addon").html("Rp. 0");
                $("#total").html("Rp. "+total);
            });

            
            
        </script>
    </body>
</html>