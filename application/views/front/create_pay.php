<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Zuwaja - Undangan Nikah dan Sumbangan Nikah Online</title>
        <?php $this->load->view('front/parts/head') ?>
    </head>
    <body>
        
        <?php $this->load->view('front/parts/header') ?>

        <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="ftco-animate mt-5">
                        <h3 class="text-white">Buat Undangan</h3>
                        <p class="breadcrumbs mb-0">
                            <span class="mr-3"><a href="<?= base_url('create') ?>">Pilih tema <i class="ion-ios-arrow-forward"></i></a></span> 
                            <span class="mr-3"><a href="<?= base_url('create/addon') ?>">Pilih paket tambahan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3"><a href="<?= base_url('create/data') ?>">Edit Undangan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3"><a href="<?= base_url('create/pay') ?>">Pembayaran <i class="ion-ios-arrow-forward"></i></a></span>
                            <span>Undangan Terbit</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="ftco-section bg-light pb-5 pt-5">
            <div class="container ftco-animate">
                <form action="#" method="get">
                    <div class="row">
                        <div class="col-md-9 mx-auto">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Pembayaran</h5>
                                </div>
                                <div class="card-body border-bottom">
                                    <strong>Rincian Belanja</strong>
                                    <table class="table table-cart text-left" >
                                        <tbody>
                                            <tr>
                                                <td>Template</td>
                                                <td>Rp. 50,000</td>
                                            </tr>
                                            <tr>
                                                <td>Addon</td>
                                                <td>Rp. 50,000</td>
                                            </tr>
                                            <tr class="total">
                                                <td>Total</td>
                                                <td>Rp. 100,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-body">
                                    silahkan pilih opsi pembayaran dibawah ini
                                    <table class="table table-payment text-left" style="background-color:#fff !important;">
                                        <tr>    
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio1">
                                                        <h5>BNI</h5>
                                                        <span>Hanya mendukung pembayaran Bank BNI melalui transfer ATM, Internet Banking dan Mobile Banking</span>
                                                    </label>
                                                </div>
                                            </td>

                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/bni.png') ?>" alt="" style="height:30px">
                                            </td>
                                        </tr>

                                        <tr>    
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio2">
                                                        <h5>Mandiri</h5>
                                                        <span>Hanya mendukung pembayaran Bank Mandiri melalui transfer ATM, Internet Banking dan Mobile Banking</span>
                                                    </label>
                                                </div>
                                            </td>

                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/mandiri.webp') ?>" alt="" style="height:30px">
                                            </td>
                                        </tr>

                                        <tr>    
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio3">
                                                        <h5>Mandiri</h5>
                                                        <span>Hanya mendukung pembayaran Bank BRI melalui transfer ATM, Internet Banking dan Mobile Banking</span>
                                                    </label>
                                                </div>
                                            </td>

                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/bri.png') ?>" alt="" style="height:30px">
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio4">
                                                        <h5>Bank lainnya/ATM Bersama</h5>
                                                        <span>Mendukung semua transfer bank di Indonesia melalui ATM / Internet Banking / Mobile Banking maupun SMS Banking</span>
                                                    </label>
                                                    
                                                </div>
                                            </td>
                                            
                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/atmbersama.png') ?>" alt="" style="height:50px">
                                            </td>    
                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio5" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio5"><h5>OVO</h5></label>
                                                </div>
                                            </td>
                                            
                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/ovo.jpg') ?>" alt="" style="height:50px">
                                            </td>    
                                        </tr>
                                        <!-- 
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio6" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio6"><h5>Gopay</h5></label>
                                                </div>
                                            </td>
                                            
                                            <td class="text-right">
                                                <img src="<?= base_url('assets/front/images/gopay.png') ?>" alt="" style="height:50px">
                                            </td>    
                                        </tr> -->
                                    </table>
                                    <button type="submit" class="btn btn-primary float-right">Lanjutkan Pembayaran</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <?php $this->load->view('front/parts/footer'); ?>
        <?php $this->load->view('front/parts/script'); ?>
        <script>
            $( document ).ready(function() {
                
            });
            
        </script>
    </body>
</html>