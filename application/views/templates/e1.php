
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wedding <?= $wedding['nickname_man'] ?> dan <?= $wedding['nickname_woman'] ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
    -->
    
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
	
	<link href="https://fonts.googleapis.com/css?family=Montez" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/superfish.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/magnific-popup.css">
	
	<link rel="stylesheet" href="<?= base_url('assets/template/e/') ?>css/style.css">
    
    <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700&display=swap&subset=arabic" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

	<!-- Modernizr JS -->
	<script src="<?= base_url('assets/template/e/') ?>js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
    <![endif]-->
    
    <style>
        .fh5co-overlay {
            position: absolute !important;
            width: 100%;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 1;
            background: rgba(0, 0, 0, 0.75);
        }
        .card-resepsi {
            text-align: center;
            background-color: #fff;
            padding: 25px;
            border-radius: 20px;
        }
        .card-resepsi h3 {
            font-size: 36pt !important;
        }
        .card-resepsi h5 {
            font-size: 26pt !important;
        }
        .mobile-bottom-bar {
            display: none;
        }

        @media screen and (max-width: 480px) {
			.mobile-bottom-bar {
				width: 100%;
				position: fixed;
				bottom: 0;
				z-index: 9999;
				padding: 5px;
				background: #fff;
				border-top: solid 1px #cfcfcf;
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;
				-webkit-justify-content: center;
					-ms-flex-pack: center;
						justify-content: center;
				-webkit-align-items: center;
					-ms-flex-align: center;
						align-items: center;
			}

			.mobile-bottom-bar .footer-link {
				-webkit-flex: 1 1 auto;
					-ms-flex: 1 1 auto;
						flex: 1 1 auto;
				text-align: center;
				color: #3d3d3d;
				text-transform: uppercase;
				font-size: 2rem;
				font-weight: bold;
				padding: 0;
				color:rgba(245, 28, 122, 0.8) !important;
			}

			.mobile-bottom-bar .footer-link i.fa {
				opacity: 0.8;
				margin-right: 0.625rem;
				font-size: 1.5rem;
				vertical-align: middle;
			}

			.mobile-bottom-bar .footer-link:focus, .mobile-bottom-bar .footer-link:active {
				color: #3d3d3d;
			}

			.mobile-bottom-bar .footer-text {
				position: relative;
				font-weight: bold;
				font-size: 8pt;
				color: #3d3d3d;
				margin-top: 0px;
				margin-bottom: 0 !important;
			}
			.banner_content img {
				display: block !important;
				width: 100%;
			}
			.banner_content h3 {
				font-size: 28pt !important;
			}
		}
    </style>

	</head>
	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
	
		<div class="fh5co-hero" id="home" data-section="home">
            <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url(<?= $wedding['foto_sampul'] ?>);">
                <div class="fh5co-overlay"></div>
				<div class="display-t">
					<div class="display-tc">
						<div class="container">
							<div class="col-md-10 col-md-offset-1">
								<div class="animate-box">
									<h1>The Wedding</h1>
									<h2><?= $wedding['nickname_man'] ?> &amp; <?= $wedding['nickname_woman'] ?></h2>
									<p><span><?= date('d.m.Y', strtotime($wedding['tanggal_resepsi'])) ?></span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:header-top -->
		
		<div id="fh5co-couple" class="fh5co-section-gray">
			<div class="container">
				<div class="row row-bottom-padded-md animate-box">
					<div class="col-md-6 col-md-offset-3 text-center">
						<div class="col-md-5 col-sm-5 col-xs-5 nopadding img-pasangan">
							<img src="<?= $wedding['foto_pria'] ?>" class="img-responsive" id="imgPs">
                            <h3><?= $wedding['fullname_man'] ?></h3>
                            <p><?= $wedding['desc_man'] ?></p>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2 nopadding"><h2 class="amp-center"><i class="icon-heart"></i></h2></div>
						<div class="col-md-5 col-sm-5 col-xs-5 nopadding">
							<img src="<?= $wedding['foto_wanita'] ?>" class="img-responsive" id="imgPs2">
                            <h3><?= $wedding['fullname_woman'] ?></h3>
                            <p><?= $wedding['desc_woman'] ?></p>
						</div>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-12 text-center heading-section">
							<h2>Are Getting Married</h2>
							<p><strong><?= date('d F Y', strtotime($wedding['tanggal_resepsi'])).' &mdash; '.$wedding['kota'] ?> </strong></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-started" style="background-image:url(<?= base_url('assets/template/c/images/bg6.JPG') ?>);" data-stellar-background-ratio="0.5">
			<div class="container">
                <div class="text-center">
                    <?php if($wedding['withBasmallah'] == 1){ ?>
                        <h4 style="font-family: 'Tajawal', sans-serif;">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</h4>
                        <h2 style="font-size: 42pt !important;">Assalamu'alaikum</h2>
                    <?php } else {  ?>
                        <h2>You are invited</h2>
                    <?php } ?>
                    <p>We invited you to celebrate our wedding</p>
                    <br>
                </div>
                <div class="col-md-6">
                    <div class="card card-resepsi card-body">
                        <h3>Akad Nikah</h3>
                        <br>
                        <span><?= date('l', strtotime($wedding['tanggal_akad_nikah'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_akad_nikah'])); ?> <br><?= $wedding['jam_akad_nikah'] ?></span>
                        <hr>
                        <h5><?= $wedding['lokasi_akad_nikah'] ?></h5>
                        <p><?= $wedding['alamat_akad_nikah'] ?></p>
                        <br>
                        <div class="date">
                            <a href="<?= $wedding['maps_akad_nikah'] ?>" class="btn btn-primary" target="_blank" >Lihat Maps</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card card-resepsi card-body">
                        <h3>Resepsi</h3>
                        <br>
                        <span><?= date('l', strtotime($wedding['tanggal_resepsi'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_resepsi'])); ?> <br><?= $wedding['jam_resepsi'] ?></span>
                        <hr>
                        <h5><?= $wedding['lokasi_resepsi'] ?></h5>
                        <p><?= $wedding['alamat_resepsi'] ?></p>
                        <br>
                        <div class="date">
                            <a href="<?= $wedding['maps_resepsi'] ?>" class="btn btn-primary" target="_blank" >Lihat Maps</a>
                        </div>
                    </div>
                </div>
			</div>
		</div>
        
        <div id="fh5co-gallery">
			<div class="container">
                <?php if($wedding['withGaleri'] == 1){ ?>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
						<h2>Wedding Gallery</h2>
					</div>
				</div>
				<div class="row">
                    <?php 
                    $get_img = $this->db->get_where('invitation_gallery',array('invitation_id' => $wedding['id']));
                    $count = $get_img->num_rows();
                    foreach($get_img->result() as $i => $img): ?>
					<div class="col-md-4">
						<div class="gallery animate-box">
							<a class="gallery-img image-popup image-popup" href="<?= $img->filename ?>"><img src="<?= $img->filename ?>" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co"></a>
						</div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php } ?>
			</div>
		</div>
        
		<footer>
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<p>Copyright <?= date('Y') ?> Akadin.ID</p>
						</div>
					</div>
				</div>
			</div>
		</footer>

        <div class="mobile-bottom-bar">
            <a href="#home" class="footer-link">
                <i class="fas fa-home"></i> 
                <p class='footer-text'>Home</p>
            </a>
            <a href="#fh5co-couple" class="footer-link">
                <i class="fas fa-heart"></i>
                <p class='footer-text'>Mempelai</p>
            </a>
            <a href="#fh5co-started" class="footer-link">
                <i class="far fa-calendar-alt"></i>
                <p class='footer-text'>Acara</p>
            </a>
            <a href="#fh5co-gallery" class="footer-link">
                <i class="far fa-images"></i>
                <p class='footer-text'>Gallery</p>
            </a>
        </div>

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>

	<!-- jQuery -->
    <script src="<?= base_url('assets/template/e/') ?>dist/scripts.js"></script>
    <script>
        var cw = document.getElementById("imgPs").width;
        document.getElementById("imgPs").style.height = cw + "px";
        
        var cw2 = document.getElementById("imgPs2").width;
        document.getElementById("imgPs2").style.height = cw2 + "px";
        
        var today = new Date();

            var timer = function() {};
            timer.countdownDate = new Date('<?= date('m/d/y', strtotime($wedding['tanggal_resepsi'])) ?>');
            timer.countdownDate2 = new Date();

            // set date to 10 days in the future for testing purposes
            <?php if($this->uri->segment(1) == 'example') { ?>
                timer.countdownDate2.setDate( timer.countdownDate2.getDate() + 3 );
            <?php }else { ?>
                timer.countdownDate;
            <?php } ?>
            /*
            * Get thing started
            */
            timer.init = function() {
            timer.getReferences();
            
            
            timer.getTimes();
            setInterval(function() { timer.update() }, 1000);
            }

            /*
            * Save references of timer section
            */
            timer.getReferences = function() {
            timer.timer = document.getElementById("timer");
            timer.days = timer.timer.querySelectorAll(".days .timer__number")[0];
            timer.hours = timer.timer.querySelectorAll(".hours .timer__number")[0];
            timer.minutes = timer.timer.querySelectorAll(".minutes .timer__number")[0];
            timer.seconds = timer.timer.querySelectorAll(".seconds .timer__number")[0];
            }

            /*
            * remember time units for later use
            */
            timer.getTimes = function() {
            timer.times = {};
            timer.times.second = 1000;
            timer.times.minute = timer.times.second * 60;
            timer.times.hour = timer.times.minute * 60;
            timer.times.day = timer.times.hour * 24;
            }

            /*
            * Update the countdown
            */
            timer.update = function() {
            if ( timer.timer.style.opacity !== 1 ) {
                timer.timer.style.opacity = 1;
            }
            
            timer.currentDate = new Date();
            timer.difference = timer.countdownDate - timer.currentDate;
            
            timer.days.innerHTML = timer.getTimeRemaining(timer.times.day, 1);
            timer.hours.innerHTML = timer.getTimeRemaining(timer.times.hour, 24);
            timer.minutes.innerHTML = timer.getTimeRemaining(timer.times.minute, 60);
            timer.seconds.innerHTML = timer.getTimeRemaining(timer.times.second, 60);
            }

            /*
            * calculate remaining time based on a unit of time
            */
            timer.getTimeRemaining = function( timeUnit, divisor ) {
            var n;
            if ( divisor == 1 ) {
                n = Math.floor(timer.difference / timeUnit );
            }
            else {
                n = Math.floor((timer.difference / timeUnit) % divisor );
            }
            
            if ( String(n).length < 2 ) {
                n = "0" + n;
            }
            
            return n;
            }

            window.addEventListener("load", function() {
            timer.init();
            });
    </script>
	</body>
</html>

