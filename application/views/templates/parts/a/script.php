<!-- jQuery -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url('assets/template/a/') ?>js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?= base_url('assets/template/a/') ?>js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.countTo.js"></script>

	<!-- Stellar -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.stellar.min.js"></script>
	<!-- Magnific Popup -->
	<script src="<?= base_url('assets/template/a/') ?>js/jquery.magnific-popup.min.js"></script>
	<script src="<?= base_url('assets/template/a/') ?>js/magnific-popup-options.js"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="<?= base_url('assets/template/a/') ?>js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="<?= base_url('assets/template/a/') ?>js/main.js"></script>