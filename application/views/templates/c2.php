<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Wedding <?= $wedding['nickname_man'] ?> dan <?= $wedding['nickname_woman'] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Libre+Caslon+Text:400,400i,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/animate.css">
    
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/magnific-popup.css">

    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/aos.css">

    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/ionicons.min.css">
    
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/flaticon.css">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/icomoon.css">
    <link rel="stylesheet" href="<?= base_url('assets/template/c/') ?>css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    <style>
    .mobile-bottom-bar {
        display: none;
    }

    .video-hero .overlay {
        opacity: 0 !important;
    }

    .couple-half.ch2{
        width: 50%;
        float: left;
    }

    .couple-wrap {
        width: 90%;
        margin: 0 auto;
        position: relative;
    }

    .couple-half h3 {
        font-family: "Sacramento", Arial, serif;
        color: #F14E95;
        font-size: 30px;
    }
    .couple-half .groom, .couple-half .bride {
        float: left;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        border-radius: 50%;
        width: 150px;
        height: 150px;
    }
    .couple-half .groom img, .couple-half .bride img {
        width: 150px;
        height: 150px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        border-radius: 50%;
    }
    .couple-half .groom {
        float: right;
        margin-right: 5px;
    }
    .couple-half .bride {
        float: left;
        margin-left: 5px;
    }
    .couple-half .desc-groom {
        padding-right: 180px;
        text-align: right;
    }
    .couple-half .desc-bride {
    padding-left: 180px;
    text-align: left;
    }

    .heart {
        position: absolute;
        top: 3em;
        left: 0;
        right: 0;
        z-index: 99;
    }
    .heart i {
        font-size: 20px;
        background: #fff;
        padding: 20px;
        color: #F14E95;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        border-radius: 50%;
    }
    @media screen and (max-width: 480px) {
        .mobile-bottom-bar {
            width: 100%;
            position: fixed;
            bottom: 0;
            z-index: 99;
            padding: 5px;
            background: #fff;
            border-top: solid 1px #cfcfcf;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-justify-content: center;
                -ms-flex-pack: center;
                    justify-content: center;
            -webkit-align-items: center;
                -ms-flex-align: center;
                    align-items: center;
        }

        .mobile-bottom-bar .footer-link {
            -webkit-flex: 1 1 auto;
                -ms-flex: 1 1 auto;
                    flex: 1 1 auto;
            text-align: center;
            color: #3d3d3d;
            text-transform: uppercase;
            font-size: 1.8rem;
            font-weight: bold;
            padding: 0;
            color:rgba(245, 28, 122, 0.8) !important;
        }

        .mobile-bottom-bar .footer-link i.fa {
            opacity: 0.8;
            margin-right: 0.625rem;
            font-size: 1.5rem;
            vertical-align: middle;
        }

        .mobile-bottom-bar .footer-link:focus, .mobile-bottom-bar .footer-link:active {
            color: #3d3d3d;
        }

        .mobile-bottom-bar .footer-text {
            position: relative;
            font-weight: bold;
            font-size: 0.8rem;
            color: #3d3d3d;
            margin-top: -5px;
            margin-bottom: 0 !important;
        }

        .video-hero .text h1 {
            font-size: 36pt !important;
        }
        .video-hero .text .subheading {
            font-size: 11px;
            letter-spacing: 3px;
            margin-bottom: 10px;
        }

        #timer .time {
            font-size: 20px !important;
        }

        #timer .time span{
            font-size: 10px !important;
        }
    }
    </style>
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

	  <section id="home" class="video-hero js-fullheight" style="height: 100vh; background-image: url(<?= base_url('assets/template/c/images/bg6.JPG') ?>); background-size:cover; background-position: top center;" data-stellar-background-ratio="0.5">
	  	<div class="overlay"></div>
			<div class="container">
				<div class="row js-fullheight justify-content-center d-flex align-items-center">
					<div class="col-md-12">
						<div class="text text-center">
							<span class="subheading" style="color: #000 !important">Wedding Invitation</span>
                            <h1 style="color: #000 !important"><?= $wedding['nickname_man'] ?> &amp; <?= $wedding['nickname_woman'] ?></h1>
                            <div class="couple-wrap animate-box">
                                <div class="couple-half ch2">
                                    <div class="groom">
                                        <img src="<?= $wedding['foto_pria'] ?>" alt="groom" class="img-responsive">
                                    </div>
                                </div>
                                <p class="heart hrt2 text-center"><i class="fas fa-heart"></i></p>
                                <div class="couple-half ch2">
                                    <div class="bride">
                                        <img src="<?= $wedding['foto_wanita'] ?>" alt="groom" class="img-responsive">
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</section>
    </section>

    <section class="ftco-section bg-section" id="mempelai">
    	<div class="overlay-top" style="background-image: url(<?= base_url('assets/template/c/') ?>images/top-bg.jpg);"></div>
    	<div class="overlay-bottom" style="background-image: url(<?= base_url('assets/template/c/') ?>images/bottom-bg.jpg);"></div>
    	<div class="container">
    		
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
		        	<div class="col-md-6 text-center d-flex align-items-stretch">
		        		<div class="bride-groom ftco-animate">
		        			<div class="img" style="background-image: url(<?= $wedding['foto_pria'] ?>);"></div>
		        			<div class="text mt-4 px-4">
		        				<h2><?= $wedding['fullname_man'] ?></h2>
		        				<p><?= $wedding['desc_man'] ?></p>
		        			</div>
		        		</div>
		        	</div>
		        	<div class="col-md-6 text-center d-flex align-items-stretch">
		        		<div class="bride-groom ftco-animate">
		        			<div class="img" style="background-image: url(<?= $wedding['foto_wanita'] ?>);"></div>
		        			<div class="text mt-4 px-4">
		        				<h2><?= $wedding['fullname_woman'] ?></h2>
		        				<p><?= $wedding['desc_woman'] ?></p>
		        			</div>
		        		</div>
		        	</div>
		        </div>
		      </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section bg-light" id="when-where-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 text-center heading-section ftco-animate">
          	<span class="clone">Waktu dan Tempat</span>
            <h2 class="mb-3">Waktu &amp; Tempat</h2>
          </div>
        </div>
        <div class="row">
            <?php if($wedding['withAkad'] == 1) { ?>
        	<div class="col-md-6 mx-auto ftco-animate">
        		<div class="place img" style="background-image: url(images/place-1.jpg);">
        			<div class="text text-center">
	        			<span class="icon flaticon-reception-bell"></span>
	        			<h3>Akad</h3>
                        <p><span><?= date('l', strtotime($wedding['tanggal_akad_nikah'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_akad_nikah'])); ?></span><br><span><?= $wedding['jam_akad_nikah'] ?></span></p>
                        <p><a href="<?= $wedding['maps_akad_nikah'] ?>" target="blank"><?= $wedding['lokasi_akad_nikah'] ?></a></p>
	        			<p><span><?= $wedding['alamat_akad_nikah'] ?></span></p>
	        			
	        			<p><a href="<?= $wedding['maps_akad_nikah'] ?>" target="_blank" class="btn-custom">Lihat Peta</a></p>
	        		</div>
        		</div>
            </div>
            <?php } ?>
        	<div class="col-md-6 mx-auto ftco-animate">
        		<div class="place img" style="background-image: url(images/place-1.jpg);">
        			<div class="text text-center">
	        			<span class="icon flaticon-wedding-kiss"></span>
	        			<h3>Resepsi</h3>
                        <p><span><?= date('l', strtotime($wedding['tanggal_resepsi'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_resepsi'])); ?></span><br><span><?= $wedding['jam_resepsi'] ?></span></p>
                        <p><a href="<?= $wedding['maps_resepsi'] ?>" target="blank"><?= $wedding['lokasi_resepsi'] ?></a></p>
	        			<p><span><?= $wedding['alamat_resepsi'] ?></span></p>
	        			
	        			<p><a href="<?= $wedding['maps_resepsi'] ?>" target="_blank" class="btn-custom">Lihat Peta</a></p>
	        		</div>
        		</div>
            </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section" id="gallery-section">
        <?php if($wedding['withGaleri'] == 1){ ?>
    	<div class="container-fluid px-md-4">
    		<div class="row justify-content-center pb-5">
                <div class="col-md-12 text-center heading-section ftco-animate">
          	        <span class="clone">Photos</span>
                    <h2 class="mb-3">Gallery</h2>
                </div>
            </div>
            <div class="row">
                <?php 
                $get_img = $this->db->get_where('invitation_gallery',array('invitation_id' => $wedding['id']));
                $count = $get_img->num_rows();
                foreach($get_img->result() as $i => $img): ?>
                <div class="col-md-3 ftco-animate">
                    <a href="<?= $img->filename ?>" class="gallery img image-popup d-flex align-items-center justify-content-center" style="background-image: url(<?= $img->filename ?>);">
                        <div class="icon d-flex align-items-center justify-content-center"><span class="ion-ios-image"></span></div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php } ?>
    </section>
    
    <div class="mobile-bottom-bar">
        <a href="#home" class="footer-link">
            <i class="fas fa-home"></i> 
            <p class='footer-text'>Home</p>
        </a>
        <a href="#mempelai" class="footer-link">
            <i class="fas fa-heart"></i>
            <p class='footer-text'>Mempelai</p>
        </a>
        <a href="#when-where-section" class="footer-link">
            <i class="far fa-calendar-alt"></i>
            <p class='footer-text'>Acara</p>
        </a>
        <a href="#gallery-section" class="footer-link">
            <i class="far fa-images"></i>
            <p class='footer-text'>Gallery</p>
        </a>
    </div>
  	

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?= base_url('assets/template/c/') ?>js/jquery.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/popper.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/bootstrap.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.easing.1.3.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.waypoints.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.stellar.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/owl.carousel.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.magnific-popup.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/aos.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.animateNumber.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/jquery.mb.YTPlayer.min.js"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="<?= base_url('assets/template/c/') ?>js/google-map.js"></script>
  
  <script src="<?= base_url('assets/template/c/') ?>js/main.js"></script>

  <script>
    function makeTimer() {

    var endTime = new Date('<?= date('m/d/y', strtotime($wedding['tanggal_resepsi'])) ?>');			
    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400); 
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    $("#days").html(days + "<span>Days</span>");
    $("#hours").html(hours + "<span>Hours</span>");
    $("#minutes").html(minutes + "<span>Minutes</span>");
    $("#seconds").html(seconds + "<span>Seconds</span>");		

    }

    setInterval(function() { makeTimer(); }, 1000);
  </script>
    
  </body>
</html>