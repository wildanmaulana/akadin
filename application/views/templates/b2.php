<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>Wedding <?= $wedding['nickname_man'] ?> dan <?= $wedding['nickname_woman'] ?></title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>vendors/linericon/style.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>vendors/animate-css/animate.css">
        <!-- main css -->
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>css/style.css">
        <link rel="stylesheet" href="<?= base_url('assets/template/b/') ?>css/responsive.css">
        <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700&display=swap&subset=arabic" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

        <style>

            .home_banner_area .banner_inner .overlay {
                background-color: #fff;
                background-size: 100%;
                position: absolute;
                left: 0;
                right: 0;
                top: 0;
                height: 100%;
                bottom: 0;
                z-index: -1;
                opacity: 0.1;
            }

            .journey_item img {
                border-radius: 100%;
            }

            .mobile-bottom-bar {
                display: none;
            }

            .home_banner_area .banner_inner .banner_content h5 {
                margin-top: -5px !important;
                font-size: 16pt;
            }

            .home_banner_area .banner_inner .banner_content h3 {
                font-size: 46pt;
            }

            @media screen and (max-width: 480px) {
                .journey_item img {
                    display: none;
                }
                .mobile-bottom-bar {
                    width: 100%;
                    position: fixed;
                    bottom: 0;
                    z-index: 99;
                    padding: 5px;
                    background: #fff;
                    border-top: solid 1px #cfcfcf;
                    display: -webkit-flex;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-justify-content: center;
                        -ms-flex-pack: center;
                            justify-content: center;
                    -webkit-align-items: center;
                        -ms-flex-align: center;
                            align-items: center;
                }

                .mobile-bottom-bar .footer-link {
                    -webkit-flex: 1 1 auto;
                        -ms-flex: 1 1 auto;
                            flex: 1 1 auto;
                    text-align: center;
                    color: #3d3d3d;
                    text-transform: uppercase;
                    font-size: 1.5rem;
                    font-weight: bold;
                    padding: 0;
                    color:rgba(245, 28, 122, 0.8) !important;
                }

                .mobile-bottom-bar .footer-link i.fa {
                    opacity: 0.8;
                    margin-right: 0.625rem;
                    font-size: 1.5rem;
                    vertical-align: middle;
                }

                .mobile-bottom-bar .footer-link:focus, .mobile-bottom-bar .footer-link:active {
                    color: #3d3d3d;
                }

                .mobile-bottom-bar .footer-text {
                    position: relative;
                    font-weight: bold;
                    font-size: 8pt;
                    color: #3d3d3d;
                    margin-top: 0px;
                    margin-bottom: 0 !important;
                }
                .banner_content img {
                    display: block !important;
                    width: 100%;
                }
                .banner_content h3 {
                    font-size: 28pt !important;
                }
            }
        </style>
    </head>
    <body>
        
        <!--================Home Banner Area =================-->
        <section class="home_banner_area" id="home">
            <div class="banner_inner d-flex align-items-center" style="background: url(<?= base_url('assets/template/a/') ?>images/bg4.jpg) no-repeat scroll center top; background-size: cover; z-index: -1;">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center" style="z-index: 11 !important">
						<img src="<?= base_url('assets/template/b/') ?>img/or2-1.png" alt="">
						<h5 style="color: #ff2f92 !important">The Wedding of</h5>
						<h3><?= $wedding['nickname_man'] ?> & <?= $wedding['nickname_woman'] ?></h3>
						<img src="<?= base_url('assets/template/b/') ?>img/or2-2.png" alt="">
					</div>
				</div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Place Area =================-->
        <section class="act_area p_120" id="acara">
        	<div class="container">
                <div class="text-center">
                    <div class="row journey_inner">
                        <div class="col-lg-3">
                            <div class="journey_image_items">
                                <div class="journey_item">
                                    <img class="img-fluid" src="<?= $wedding['foto_pria'] ?>" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="journey_text_items">
                                <div class="journey_item">
                                    <?php if($wedding['withBasmallah'] == 1){ ?>
                                        <h4 style="font-family: 'Tajawal', sans-serif;">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</h4>
                                        <h2>Assalamu'alaikum</h2>
                                    <?php } else {  ?>
                                        <h2>You are invited</h2>
                                    <?php } ?>
                                    <p>We invited you to celebrate our wedding</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="journey_image_items">
                                <div class="journey_item">
                                    <img class="img-fluid" src="<?= $wedding['foto_wanita'] ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
        		<div class="row act_inner">
                    <?php if($wedding['withAkad'] == 1) { ?>
        			<div class="col-lg-6">
        				<div class="journey_text_items">
        					<div class="journey_item">
        						<h4>Akad Nikah</h4>
                                <h5><?= date('l', strtotime($wedding['tanggal_akad_nikah'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_akad_nikah'])); ?> <br><?= $wedding['jam_akad_nikah'] ?></h5>
                                <h5><?= $wedding['lokasi_akad_nikah'] ?></h5>
                                <p><?= $wedding['alamat_akad_nikah'] ?></p>
        						<div class="date">
        							<a href="<?= $wedding['maps_akad_nikah'] ?>" target="_blank" >Lihat Maps</a>
        						</div>
        					</div>
        				</div>
                    </div>
                    <?php } ?>
        			<div class="col-lg-6 <?php if($wedding['withAkad'] == 0) echo "mx-auto" ?>">
                        <div class="journey_text_items">
        					<div class="journey_item">
        						<h4>Resepsi</h4>
                                <h5><?= date('l', strtotime($wedding['tanggal_resepsi'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_resepsi'])); ?> <br><?= $wedding['jam_resepsi'] ?></h5>
                                <h5><?= $wedding['lokasi_resepsi'] ?></h5>
                                <p><?= $wedding['alamat_resepsi'] ?></p>
        						<div class="date">
        							<a href="<?= $wedding['maps_resepsi'] ?>" target="_blank" >Lihat Maps</a>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Place Area =================-->

        <!--================Story Area =================-->
        <section class="love_journey_area p_120" id="mempelai">
        	<div class="container">
        		<div class="love_inner">
        			<div class="love_js_items">
        				<div class="love_js_item row">
        					<div class="col-lg-7">
								<div class="love_text_item">
									<h4><?= $wedding['fullname_man'] ?></h4>
									<p><?= $wedding['desc_man'] ?></p>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="love_img"><img class="img-fluid" src="<?= $wedding['foto_pria'] ?>" alt=""></div>
							</div>
        				</div>
        				<div class="love_js_item right row">
							<div class="col-lg-5">
								<div class="love_img"><img class="img-fluid" src="<?= $wedding['foto_wanita'] ?>" alt=""></div>
							</div>
       						<div class="col-lg-7">
								<div class="love_text_item">
									<h4><?= $wedding['fullname_woman'] ?></h4>
									<p><?= $wedding['desc_woman'] ?></p>
								</div>
							</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Story Area =================-->
        
        <!--================Timer Area =================-->
        <section class="timer_area">
        	<div class="container box_1620">
        		<div class="timer_inner p_120">
        			<h5>Until we getting married</h5>
        			<div id="timer" class="timer">
						<div class="timer__section days">
							<div class="timer__number"></div>
							<div class="timer__label">days</div>
						</div>
						<div class="timer__section hours">
							<div class="timer__number"></div>
							<div class="timer__label">hours</div>
						</div>
						<div class="timer__section minutes">
							<div class="timer__number"></div>
							<div class="timer__label">Minutes</div>
						</div>
						<div class="timer__section seconds">
							<div class="timer__number"></div>
							<div class="timer__label">seconds</div>
						</div>
					</div>
       				<p>are remaining</p>
        		</div>
        	</div>
        </section>
        <!--================End Timer Area =================-->
        
        <!--================Moments Area =================-->
        <section class="moments_area mt-5 mb-5" id="gallery">
            <?php if($wedding['withGaleri'] == 1){ ?>
        	<div class="container box_1620">
        		<div class="main_title">
        			<h2>Special Moments Captured</h2>
        		</div>
        		<div class="moments_inner imageGallery1">
                    <?php 
                    $get_img = $this->db->get_where('invitation_gallery',array('invitation_id' => $wedding['id']));
                    $count = $get_img->num_rows();
                    foreach($get_img->result() as $i => $img): ?>
        			<div class="gallery_item">
						<div class="h_gallery_item">
							<img src="<?= $img->filename ?>" alt="">
							<div class="hover">
								<a class="light" href="<?= $img->filename ?>"><i class="fa fa-expand"></i></a>
							</div>
						</div>
                    </div>
                    <?php endforeach; ?> 
        		</div>
            </div>
            <?php } ?>
        </section>
        <!--================End Moments Area =================-->
                
        <?php 
		$data['invitation_id'] = $wedding['invitation_id'];
		$this->load->view('templates/parts/comments',$data); ?>
        
        <!--================Footer Area =================-->
        <footer class="footer_area">
        	<div class="container">
        		<div class="footer_inner">
        			
       				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Akadin</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        		</div>
        	</div>
        </footer>

        <div class="mobile-bottom-bar">
            <a href="#home" class="footer-link">
                <i class="fas fa-home"></i> 
                <p class='footer-text'>Home</p>
            </a>
            <a href="#mempelai" class="footer-link">
                <i class="fas fa-heart"></i>
                <p class='footer-text'>Mempelai</p>
            </a>
            <a href="#acara" class="footer-link">
                <i class="far fa-calendar-alt"></i>
                <p class='footer-text'>Acara</p>
            </a>
            <a href="#gallery" class="footer-link">
                <i class="far fa-images"></i>
                <p class='footer-text'>Gallery</p>
            </a>
        </div>
        <!--================End Footer Area =================-->
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?= base_url('assets/template/b/') ?>js/jquery-3.2.1.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/popper.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/stellar.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>vendors/isotope/isotope-min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/jquery.ajaxchimp.min.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/mail-script.js"></script>
        <script src="<?= base_url('assets/template/b/') ?>js/theme.js"></script>

        <script>
            var cw = $('.journey_item img').width();
            $('.journey_item img').css({'height':cw+'px'});

            var today = new Date();

            var timer = function() {};
            timer.countdownDate = new Date('<?= date('m/d/y', strtotime($wedding['tanggal_resepsi'])) ?>');
            timer.countdownDate2 = new Date();

            // set date to 10 days in the future for testing purposes
            <?php if($this->uri->segment(1) == 'example') { ?>
                timer.countdownDate2.setDate( timer.countdownDate2.getDate() + 3 );
            <?php }else { ?>
                timer.countdownDate;
            <?php } ?>
            /*
            * Get thing started
            */
            timer.init = function() {
            timer.getReferences();
            
            
            timer.getTimes();
            setInterval(function() { timer.update() }, 1000);
            }

            /*
            * Save references of timer section
            */
            timer.getReferences = function() {
            timer.timer = document.getElementById("timer");
            timer.days = timer.timer.querySelectorAll(".days .timer__number")[0];
            timer.hours = timer.timer.querySelectorAll(".hours .timer__number")[0];
            timer.minutes = timer.timer.querySelectorAll(".minutes .timer__number")[0];
            timer.seconds = timer.timer.querySelectorAll(".seconds .timer__number")[0];
            }

            /*
            * remember time units for later use
            */
            timer.getTimes = function() {
            timer.times = {};
            timer.times.second = 1000;
            timer.times.minute = timer.times.second * 60;
            timer.times.hour = timer.times.minute * 60;
            timer.times.day = timer.times.hour * 24;
            }

            /*
            * Update the countdown
            */
            timer.update = function() {
            if ( timer.timer.style.opacity !== 1 ) {
                timer.timer.style.opacity = 1;
            }
            
            timer.currentDate = new Date();
            timer.difference = timer.countdownDate - timer.currentDate;
            
            timer.days.innerHTML = timer.getTimeRemaining(timer.times.day, 1);
            timer.hours.innerHTML = timer.getTimeRemaining(timer.times.hour, 24);
            timer.minutes.innerHTML = timer.getTimeRemaining(timer.times.minute, 60);
            timer.seconds.innerHTML = timer.getTimeRemaining(timer.times.second, 60);
            }

            /*
            * calculate remaining time based on a unit of time
            */
            timer.getTimeRemaining = function( timeUnit, divisor ) {
            var n;
            if ( divisor == 1 ) {
                n = Math.floor(timer.difference / timeUnit );
            }
            else {
                n = Math.floor((timer.difference / timeUnit) % divisor );
            }
            
            if ( String(n).length < 2 ) {
                n = "0" + n;
            }
            
            return n;
            }

            window.addEventListener("load", function() {
            timer.init();
            });
        </script>
    </body>
</html>