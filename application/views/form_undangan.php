<?php echo form_open_multipart('invitation/save','id="formAdd"'); ?>
    <?php 
    if(isset($_GET['r'])) {
        $r = 1;
    } else {
        $r = 0;
    }

    if(isset($_GET['t'])) {
        $t = $this->input->get('t');
    } else {
        $t = "";
    }
    ?>
    <input type="hidden" name="option" value="<?php if($this->uri->segment(3) == 'edit')  echo "edit";
                                                    if($this->uri->segment(2) == 'add') echo "add"; ?>" />
    <input type="hidden" name="id" value="<?= $w['invitation_id'] ?>" />

    <input type="hidden" name="r" value="<?= $r ?>" />
    <div class="row">
        <div class="col-md-6 form-group">
            <label>Nama Undangan</label>   
            <input type="text" class="form-control" name="name" value="<?= $w['name'] ?>" required>
        </div>
        <?php if($this->uri->segment(1) != 'create' ) { ?>
        <div class="col-md-2 form-group">
            <label for="">Template</label>
            <select name="template" id="" class="form-control" required>
                <?php foreach($this->db->get('invitation_templates')->result() as $tmp): ?>
                <option value="<?= $tmp->filename ?>" <?php if($w['template'] == $tmp->filename OR $t == $tmp->filename) { echo "selected"; } ?> /><?= $tmp->filename ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php } ?>
        <div class="<?php if($this->uri->segment(1) == 'create' ) { echo "col-md-6"; } else { echo "col-md-4"; }?> form-group">
            <label for="">URL | 
                    <small>zuwaja.com/...</small>
            </label>
            <?php if($this->uri->segment(3) == 'edit') { ?>
            <input type="text" class="form-control" id="url" name="" value="<?= $w['slug'] ?>" disabled>
            <input type="hidden"name="url" value="<?= $w['slug'] ?>" />
            <?php } ?>
            <?php if($this->uri->segment(2) == 'add' OR $this->uri->segment(1) == 'create' ) { ?>
            <input type="text" class="form-control" id="url" name="url" required>
            <?php } ?>
            <small>Karakter diizinkan (A-Z,a-z,0-9) dan tanpa spasi</small>
        </div>
        <div class="col-md-6 form-group">
            <label>Nama Mempelai Pendek Pria</label>   
            <input type="text" class="form-control" name="nickname_man" value="<?= $w['nickname_man'] ?>" required>
        </div>
        <div class="col-md-6 form-group">
            <label>Nama Mempelai Pendek Wanita</label>   
            <input type="text" class="form-control" name="nickname_woman" value="<?= $w['nickname_woman'] ?>" required>
        </div>
        <div class="col-md-6 form-group">
            <label>Nama Mempelai Lengkap Pria</label>   
            <input type="text" class="form-control" name="fullname_man" value="<?= $w['fullname_man'] ?>" required>
        </div>
        <div class="col-md-6 form-group">
            <label>Nama Mempelai Lengkap Wanita</label>   
            <input type="text" class="form-control" name="fullname_woman" value="<?= $w['fullname_woman'] ?>" required>
        </div>
        <div class="col-md-12 form-group">
            <label for="">Deskripsi singkat/profil pria</label>
            <textarea name="deskripsi_pria"  class="form-control"><?= $w['desc_man'] ?></textarea>
        </div>
        <div class="col-md-12 form-group">
            <label for="">Deskripsi singkat/profil wanita</label>
            <textarea name="deskripsi_wanita"  class="form-control"><?= $w['desc_woman'] ?></textarea>
        </div>
        <div class="col-md-6 form-group" id="akad">
            <label>Kota tempat resepsi</label>   
            <input type="text" class="form-control" name="kota" value="<?= $w['kota'] ?>" />
            <small class="text-danger">contoh: Yogyakarta</small>
        </div>

        <div class="col-md-12 mt-3">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="withBismillah" name="bismillah" value="1" class="custom-control-input" <?php if($w['withBasmallah'] == 1) echo "checked" ?> />
                <label class="custom-control-label" for="withBismillah">Dengan Bismillah dan Assalamu'alaikum</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="noBismillah" name="bismillah" value="0" class="custom-control-input" <?php if($w['withBasmallah'] == 0) echo "checked" ?> />
                <label class="custom-control-label" for="noBismillah">Tanpa Bismillah dan Assalamu'alaikum</label>
            </div>
            <hr>
        </div>

        <div class="col-md-6 mt-3">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="withAkad" name="akad" value="1" class="custom-control-input" <?php if($w['withAkad'] == 1) echo "checked" ?>>
                <label class="custom-control-label" id="adaAkad" for="withAkad">Dengan Akad Nikah</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="noAkad" name="akad" value="0" class="custom-control-input" <?php if($w['withAkad'] == 0) echo "checked" ?> />
                <label class="custom-control-label" id="gaAkad" for="noAkad">Tanpa Akad Nikah</label>
            </div>
        </div>
        
        <!-- kolom info akad -->
        <div class="col-md-12" id="infoAkad" <?php if($w['withAkad'] == 0) echo "style='display:none;'" ?> />
            <hr>
            <h4>Informasi Akad</h4>
            <div class="row mt-3">
                <div class="col-md-6 form-group" >
                    <label>Tanggal Akad Nikah</label>   
                    <input type="date" class="form-control" name="tanggal_akad_nikah" value="<?= $w['tanggal_akad_nikah'] ?>" />
                    <small class="text-danger">format: dd/mm/yyyy contoh: 12/08/2019</small>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Waktu/Jam</label>
                    <input type="text" class="form-control" name="jam_akad_nikah" value="<?= $w['jam_akad_nikah'] ?>" />
                    <small class="text-danger">contoh: 08.00 - 09.00</small>
                </div>
                <div class="col-md-6 form-group" >
                    <label>URL Google Maps Akad Nikah</label>   
                    <input type="text" class="form-control" name="maps_akad_nikah" value="<?= $w['maps_akad_nikah'] ?>" />
                </div>
                <div class="col-md-6 form-group" >
                    <label>Nama Lokasi Akad Nikah</label>   
                    <input type="text" class="form-control" name="lokasi_akad_nikah" value="<?= $w['lokasi_akad_nikah'] ?>" />
                    <small class="text-danger">contoh: Gedung Al Hikam </small>
                </div>
                <div class="col-md-12 form-group">
                    <label>Alamat Lengkap Akad Nikah</label>   
                    <input type="text" class="form-control" name="alamat_akad_nikah" value="<?= $w['alamat_akad_nikah'] ?>" />
                </div>
            </div>
        </div>

        <!-- kolom info resepsi -->
        <div class="col-md-12" id="infoResepsi" />
            <hr>
            <h4>Informasi Resepsi</h4>
            <div class="row mt-3">
                <div class="col-md-6 form-group" >
                    <label>Tanggal Resepsi</label>   
                    <input type="date" class="form-control" name="tanggal_resepsi" value="<?= $w['tanggal_resepsi'] ?>" />
                    <small class="text-danger">format: dd/mm/yyyy contoh: 12/08/2019</small>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Waktu/Jam</label>
                    <input type="text" class="form-control" name="jam_resepsi" value="<?= $w['jam_resepsi'] ?>" />
                    <small class="text-danger">contoh: 08.00 - 09.00</small>
                </div>
                <div class="col-md-6 form-group" >
                    <label>URL Google Maps Resepsi</label>   
                    <input type="text" class="form-control" name="maps_resepsi" value="<?= $w['maps_resepsi'] ?>" />
                </div>
                <div class="col-md-6 form-group" >
                    <label>Nama Lokasi Resepsi</label>   
                    <input type="text" class="form-control" name="lokasi_resepsi" value="<?= $w['lokasi_resepsi'] ?>" />
                    <small class="text-danger">contoh: Gedung Al Hikam </small>
                </div>
                <div class="col-md-12 form-group">
                    <label>Alamat Lengkap Resepsi</label>   
                    <input type="text" class="form-control" name="alamat_resepsi" value="<?= $w['alamat_resepsi'] ?>" />
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <hr>
            <small class="text-danger">Setiap gambar ukuran maksimal <b>1 MB</b></small>   
            <br><br>
            <div class="form-group row">
            <label for="" class="col-md-3">Foto Sampul</label>
            <div class="custom-file col-md-9">
                <input type="file" class="custom-file-input" id="customFile" name="foto_sampul">
                <label class="custom-file-label" for="customFile">Choose file</label>
                <input type="hidden" name="foto_sampul_lama" value="<?= $w['foto_sampul'] ?>" />
            </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-md-3">Foto Pria</label>
                <div class="custom-file col-md-9">
                    <input type="file" class="custom-file-input" id="customFile" name="foto_pria">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                    <input type="hidden" name="foto_pria_lama" value="<?= $w['foto_pria'] ?>" />
                </div>
            </div>
            
            <div class="form-group row">
                <label for="" class="col-md-3">Foto Wanita</label>
                <div class="custom-file col-md-9">
                    <input type="file" class="custom-file-input" id="customFile" name="foto_wanita">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                    <input type="hidden" name="foto_wanita_lama" value="<?= $w['foto_wanita'] ?>" />
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-md-3">URL Lagu (.MP3)</label>
                <input type="text" class="col-md-9 form-control" name="url_lagu" value="<?= $w['lagu'] ?>" />
            </div>
            
        </div>

        <!-- Galeri -->
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-6 mt-3">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="noGaleri" name="galeri" value="0" class="custom-control-input" <?php if($w['withGaleri'] == 0) echo "checked" ?> />
                <label class="custom-control-label" for="noGaleri">Tanpa Galeri</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="withGaleri" name="galeri" value="1" class="custom-control-input" <?php if($w['withGaleri'] == 1) echo "checked" ?>>
                <label class="custom-control-label" for="withGaleri">Dengan Galeri</label>
            </div>
        </div>

        <div class="col-md-12 mt-3" id="addgaleri" style="<?php if($w['withGaleri'] == 0) echo "display:none" ?>" />
            <p class="text-danger">Upload Galeri maksimal <b>6 gambar</b> dengan maksimal ukuran tiap gambar <b>1MB</b></p>
            <input type="file" name="galeri" id="galeri">
        </div>
        <div class="col-md-12 mt-5">
            <input type="submit" name="publish" class="btn btn-primary float-right" value="<?php if($this->uri->segment(1) != 'create' ) { echo "simpan"; } else { echo "terbitkan"; } ?>">
        </div>
        
    </div>
</form>