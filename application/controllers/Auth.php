<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
        $this->load->library('encryption');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $get_data = $this->db->get_where('users', array('username' => $username));
        if($get_data->num_rows() > 0) {
            $user = $get_data->row_array();
            if($password == $this->encryption->decrypt($user['password'])) {
                $this->session->set_userdata(array('logged_in' => true));
                $this->session->set_userdata($user);
                redirect('dasbor');
            } else {
                $this->session->set_flashdata('error','Ops, username/password salah!');
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('error','Ops, username/password salah!');
            redirect('login');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    public function loginPage()
    {
        $this->load->view('loginPage');
    }
}
