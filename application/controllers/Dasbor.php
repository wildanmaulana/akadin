<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('logged_in')) {
            redirect('login');
        }
	}
    
	public function index()
	{
        $this->db->where('status !=', '9');
        $data['invitation'] = $this->db->get('invitations');

        $data['template'] = $this->db->get('invitation_templates')->result();
		$this->load->view('dasbor',$data);
    }

    public function listInv()
    {
        if(isset($_POST['id'])) {
            $this->chageStatus();
        }
        $this->db->where('status !=', '9');
        $data['invitation'] = $this->db->get('invitations');
        $this->load->view('listInv',$data);
    }

    protected function chageStatus()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');

        if($status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }

        $newstatus = array('status' => $new_status);

        $this->db->where('id', $id);
        $this->db->update('invitations', $newstatus);

        $this->session->set_flashdata('success','status berhasil diubah');

        if($this->input->post('r') == 1){
            redirect(base_url());
        }
        
    }

    public function addInv()
    {
        $data['wedding'] = null;
        $this->load->view('addInv',$data);
    }

    public function editFormInv($id)
    {
        $this->db->select("*");
        $this->db->from("invitations");
        $this->db->join("invitation_contents","invitation_contents.invitation_id = invitations.id");
        $this->db->where("invitations.id",$id);
        $data['wedding'] = $this->db->get()->row_array();
        $this->load->view('editInv',$data);
    }

    public function delGaleriUndangan($id)
    {
        $img = $this->db->get_where('invitation_gallery', array('id' => $id))->row_array();
        $pathFile = explode(base_url(),$img['filename']);
        unlink($pathFile[1]);
        
        $this->db->delete('invitation_gallery',array('id' => $id));
        $this->session->set_flashdata('info','Gambar berhasil dihapus');
        echo "<script>window.history.back();</script>";
    }

    public function delImgInv()
    {

    }

    public function saveInv() 
    {
        $data1 = array(
            'name' => $this->input->post('name'),
            'slug' => $this->input->post('url'),
            'template' => $this->input->post('template'),
            'status' => 1,
            'date_created' => time()
        );

        if($this->input->post('option') == 'edit') {
            $this->db->where('id',$this->input->post('id'));
            $this->db->update('invitations', $data1);
            $invitation_id = $this->input->post('id');
        }
        
        if($this->input->post('option') == 'add') {
            $this->db->insert('invitations',$data1);
            $invitation_id = $this->db->insert_id();
        }

        // Upload Gambar 
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1024;

        $this->load->library('upload', $config);
        $urlPath = base_url('assets/uploads/');
        $fileData = $this->upload->data();

        if($this->upload->do_upload('foto_sampul')){
            $foto_sampul = $urlPath.$this->upload->data('file_name');    
        } else {
            $foto_sampul = $this->input->post('foto_sampul_lama');
        }
        
        if($this->upload->do_upload('foto_pria')){ 
            $foto_pria = $urlPath.$this->upload->data('file_name');
        } else {
            $foto_pria = $this->input->post('foto_pria_lama');;
        }

        if($this->upload->do_upload('foto_wanita')){ 
            $foto_wanita = $urlPath.$this->upload->data('file_name');
        } else {
            $foto_wanita = $this->input->post('foto_wanita_lama');;
        }

        if($this->upload->do_upload('foto_wanita')){ 
            $foto_wanita = $urlPath.$this->upload->data('file_name');
        } else {
            $foto_wanita = $this->input->post('foto_wanita_lama');;
        }
        
        // store all data
        $data2 = array(
            'invitation_id' => $invitation_id,
            'nickname_man' => $this->input->post('nickname_man'),
            'nickname_woman' => $this->input->post('nickname_woman'),
            'fullname_man' => $this->input->post('fullname_man'),
            'fullname_woman' => $this->input->post('fullname_woman'),
            'desc_man' => $this->input->post('deskripsi_pria'),
            'desc_woman' => $this->input->post('deskripsi_wanita'),
            'kota' => $this->input->post('kota'),
            'withAkad' => $this->input->post('akad'),
            'withBasmallah' => $this->input->post('bismillah'),
            'tanggal_akad_nikah' => $this->input->post('tanggal_akad_nikah'),
            'jam_akad_nikah' => $this->input->post('jam_akad_nikah'),
            'maps_akad_nikah' => $this->input->post('maps_akad_nikah'),
            'lokasi_akad_nikah' => $this->input->post('lokasi_akad_nikah'),
            'alamat_akad_nikah' => $this->input->post('alamat_akad_nikah'),
            'tanggal_resepsi' => $this->input->post('tanggal_resepsi'),
            'jam_resepsi' => $this->input->post('jam_resepsi'),
            'maps_resepsi' => $this->input->post('maps_resepsi'),
            'lokasi_resepsi' => $this->input->post('lokasi_resepsi'),
            'alamat_resepsi' => $this->input->post('alamat_resepsi'),
            'foto_sampul' => $this->input->post('foto_sampul'),
            'foto_pria' => $this->input->post('foto_pria'),
            'foto_wanita' => $this->input->post('foto_wanita'),
            'withGaleri' => $this->input->post('galeri'),
            'foto_sampul' => $foto_sampul,
            'foto_pria' => $foto_pria,
            'foto_wanita' => $foto_wanita,
            'lagu' => $this->input->post('url_lagu')
        );

        if($this->input->post('option') == 'edit') {
            $this->db->where('id',$this->input->post('id'));
            $this->db->update('invitation_contents',$data2);
        } 

        if($this->input->post('option') == 'add') {
            $this->db->insert('invitation_contents',$data2);
        }

        //upload gallery
        $filesCount = count($_FILES['galeri']['name']);
		for($i = 0; $i < $filesCount; $i++){
			$_FILES['file']['name']     = $_FILES['galeri']['name'][$i];
			$_FILES['file']['type']     = $_FILES['galeri']['type'][$i];
			$_FILES['file']['tmp_name'] = $_FILES['galeri']['tmp_name'][$i];
			$_FILES['file']['error']     = $_FILES['galeri']['error'][$i];
			$_FILES['file']['size']     = $_FILES['galeri']['size'][$i];
				
			// File upload configuration
			$uploadPath = './assets/uploads/';
			$fullpath = base_url().'assets/uploads/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			
			// Load and initialize upload library
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			// Upload file to server
			if($this->upload->do_upload('file')){
				// Uploaded file data
				$fileData = $this->upload->data();
				$filename[$i] = $fullpath.$fileData['file_name'];
                $this->db->insert('invitation_gallery',array('invitation_id' => $invitation_id, 'filename' => $filename[$i]));
			}
        }

        $this->session->set_flashdata('success','undangan berhasil dibuat');

        if($this->input->post('r') == 1) {
            redirect(base_url());
        } else {
            redirect(base_url('invitation/list'));
        }
    }
    

    public function delInv($id)
    {
        $del_data = array('id' => $id);
        $del_data1 = array('invitation_id' => $id);

        $this->db->delete('invitation_gallery',$del_data1);
        $this->db->delete('invitation_contents',$del_data1);
        $this->db->delete('invitations',$del_data);

        $this->session->set_flashdata('info','undangan berhasil DIHAPUS');
        redirect(base_url('invitation/list'));
    }

    public function cekslug()
	{
		if(isset($_POST['slug'])){
			$slug = $_POST['slug'];
			$val_slug = $this->db->get_where('invitations',array('slug' => $slug, 'status' => 1))->num_rows();

			if($val_slug > 0){
				$result = false;
			}else{
				$result = true;
			}
			header('Content-Type: application/json');
			echo json_encode($result);
		}
    }

    public function listTemplateUser()
    {
        $data['template'] = $this->db->get('invitation_templates')->result();
        $this->load->view('templateList',$data);
    }
    
    public function addTemplate()
    {
        $this->load->view('addTemplate');
    }

    public function storeTemplate()
    {
        // Upload Gambar 
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2048;

        $this->load->library('upload', $config);
        $urlPath = base_url('assets/uploads/');
        $fileData = $this->upload->data();

        if($this->upload->do_upload('ui_desktop')){
            $ui_desktop = $urlPath.$this->upload->data('file_name');    
        } 
        $dataTemplate = array(
            'filename' => $this->input->post('filename'),
            'ui_desktop' => $ui_desktop
        );

        $this->db->insert('invitation_templates',$dataTemplate);

        $this->session->set_flashdata('success','template berhasil disimpan');
        redirect(base_url('dasbor/listTemplate'));
    }

    public function listTemplate()
    {
        $data['template'] = $this->db->get('invitation_templates')->result();
        $this->load->view('listTemplate',$data);
    }

    public function delTemplate()
    {
        $this->db->delete('invitation_templates',array('id' => $this->uri->segment(3)));
        redirect(base_url('dasbor/listTemplate'));
    }

    public function profile()
    {
        $this->load->library('encryption');
        $ps = $this->db->get_where('users', array('username' => $this->session->userdata('username')))->row_array();
        $data['pass']  = $this->encryption->decrypt($ps['password']);
        $this->load->view('editProfile',$data);
    }

    public function changeProfile()
    {
        $this->load->library('encryption');
        if($this->input->post('oldpass') != $this->input->post('oldpass1')) {
            $this->session->set_flashdata('danger','password lama yang anda masukkan salah');
            redirect(base_url('dasbor/profile'));
        }

        if($this->input->post('pass1') != $this->input->post('pass2')) {
            $this->session->set_flashdata('danger','password yang anda masukkan tidak sama');
            redirect(base_url('dasbor/profile'));
        }

        $this->db->where('username', $this->session->userdata('usernamee'));
        $this->db->update('users', array(
            'username' => $this->input->post('username'),
            'password' => $this->encryption->encrypt($this->input->post('pass1'))
        ));

        $this->session->set_flashdata('success','profil anda berhasil diubah');
        redirect(base_url('dasbor/profile'));

    }

    public function listMusic()
    {
        $data['music'] = $this->db->get('music_list')->result();
        $this->load->view('list_music',$data);
    }

    public function saveMusic()
    {
        // Upload Music 
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'mp3';
        $config['max_size']             = 8192;

        $this->load->library('upload', $config);
        $urlPath = base_url('assets/uploads/');
        $fileData = $this->upload->data();

        if($this->upload->do_upload('musik')){
            $filename = $this->upload->data('file_name');
            $url = $urlPath.$this->upload->data('file_name');    
            $this->db->insert('music_list',array(
                'filename' => $filename,
                'url'  => $url
            ));
            $this->session->set_flashdata('success','lagu berhasil ditambahkan');
            redirect(base_url('invitation/music'));
        } else {
            $this->session->set_flashdata('danger','lagu gagal ditambahkan');
            redirect(base_url('invitation/music'));
        }
    }

    public function delMusic()
    {
        $this->db->where('id',$this->input->get('id'));
        $this->db->delete('music_list');
        $this->session->set_flashdata('success','Music berhasil dihapus');
        redirect(base_url('invitation/music'));
    }
    
    /*
    public function template()
    {
        $this->load->view('templates/a1');
    }

    public function addUsers()
    {
        $this->load->library('encryption');
        date_default_timezone_set('Asia/Jakarta');
        return $this->db->insert('users', array(
            'username'  => 'admin',
            'password'  => $this->encryption->encrypt('adminkita'),
            'role'      => 1,
            'status'    => 1,
            'date_created' => time()
        ));
    } */
}
