<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function index()
	{
        $data['invitation'] = $this->db->get('invitation_templates');
		$this->load->view('front/index',$data);
    }
    
    public function create()
    {
        if(isset($_POST['template'])) {
            redirect(base_url('create/addon'));
        }
        $data['invitation'] = $this->db->get('invitation_templates');
        $this->load->view('front/create',$data);
    }

    public function create_addon()
    {
        $data['invitation'] = $this->db->get('invitation_templates');
        $this->load->view('front/create_addon',$data);
    }

    public function create_data()
    {
        $data['invitation'] = $this->db->get('invitation_templates');
        $id = "";
        $this->db->select("*");
        $this->db->from("invitations");
        $this->db->join("invitation_contents","invitation_contents.invitation_id = invitations.id");
        $this->db->where("invitations.id",$id);
        $data['wedding'] = $this->db->get()->row_array();
        $this->load->view('front/create_data',$data);
    }

    public function create_pay()
    {
        $this->load->view('front/create_pay');
    }
}
