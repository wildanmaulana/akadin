<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invitation extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		
    }
    
	public function index()
	{
        $slug = $this->uri->segment('2');
        
        if($this->db->get_where('invitations',array('slug' => $slug, 'status' => 1))->num_rows() == 0) {
            redirect(404);
            
        }

        $this->db->select("*");
        $this->db->from("invitations");
        $this->db->join("invitation_contents","invitation_contents.invitation_id = invitations.id");
        $this->db->where("slug",$slug);
        $wedding = $this->db->get()->row_array();
        $data['wedding'] = $wedding;
        $this->load->view('templates/'.$wedding['template'],$data);
    }
    
    public function example()
	{
        $slug = $this->uri->segment('2');

        $this->db->select("*");
        $this->db->from("invitations");
        $this->db->join("invitation_contents","invitation_contents.invitation_id = invitations.id");
        $this->db->where("slug","example");
        $wedding = $this->db->get()->row_array();
        $data['wedding'] = $wedding;
        $this->load->view('templates/'.$this->uri->segment(2),$data);
    }
    
    public function saveComments()
    {
        $data_comment = array(
            'invitation_id' => $this->input->post('invitation_id'),
            'is_attend' => $this->input->post('is_attend'),
            'fullname' => $this->input->post('fullname'),
            'comment' => $this->input->post('comment')
        );

        $this->db->insert('invitation_comments',$data_comment);
        echo "<script>window.history.back();</script>";
    }
}
