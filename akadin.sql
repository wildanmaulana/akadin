-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2019 at 05:41 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akadin`
--

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `template` varchar(15) NOT NULL,
  `date_created` varchar(55) NOT NULL,
  `status` int(1) NOT NULL,
  `ket` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`id`, `name`, `slug`, `template`, `date_created`, `status`, `ket`) VALUES
(10, 'Wildan Nissa', 'example', 'a4', '1571788814', 9, 'ini adalah data contoh');

-- --------------------------------------------------------

--
-- Table structure for table `invitation_contents`
--

CREATE TABLE `invitation_contents` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `nickname_man` varchar(55) NOT NULL,
  `nickname_woman` varchar(55) NOT NULL,
  `fullname_man` varchar(55) NOT NULL,
  `fullname_woman` varchar(55) NOT NULL,
  `desc_man` varchar(255) NOT NULL,
  `desc_woman` varchar(255) NOT NULL,
  `kota` varchar(55) NOT NULL,
  `withAkad` int(1) NOT NULL,
  `withBasmallah` int(1) NOT NULL,
  `tanggal_akad_nikah` varchar(55) NOT NULL,
  `jam_akad_nikah` varchar(55) NOT NULL,
  `maps_akad_nikah` varchar(255) NOT NULL,
  `lokasi_akad_nikah` varchar(155) NOT NULL,
  `alamat_akad_nikah` varchar(255) NOT NULL,
  `tanggal_resepsi` varchar(55) NOT NULL,
  `jam_resepsi` varchar(55) NOT NULL,
  `maps_resepsi` varchar(255) NOT NULL,
  `lokasi_resepsi` varchar(155) NOT NULL,
  `alamat_resepsi` varchar(255) NOT NULL,
  `foto_sampul` varchar(255) NOT NULL,
  `foto_pria` varchar(255) NOT NULL,
  `foto_wanita` varchar(255) NOT NULL,
  `withGaleri` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_contents`
--

INSERT INTO `invitation_contents` (`id`, `invitation_id`, `nickname_man`, `nickname_woman`, `fullname_man`, `fullname_woman`, `desc_man`, `desc_woman`, `kota`, `withAkad`, `withBasmallah`, `tanggal_akad_nikah`, `jam_akad_nikah`, `maps_akad_nikah`, `lokasi_akad_nikah`, `alamat_akad_nikah`, `tanggal_resepsi`, `jam_resepsi`, `maps_resepsi`, `lokasi_resepsi`, `alamat_resepsi`, `foto_sampul`, `foto_pria`, `foto_wanita`, `withGaleri`) VALUES
(10, 10, 'Wildan ', 'Nissa', 'Wildan Maulana', 'Annisa', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Yogyakarta', 1, 1, '2020-02-20', '08.00 - 09.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', '2020-02-20', '10.00 - 11.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', 'http://localhost/akadin/assets/uploads/845adaf08a9d5ebe8a4bc4110e67def6.jpg', 'http://localhost/akadin/assets/uploads/Wildan_Maulana_CEO_2.jpg', 'http://localhost/akadin/assets/uploads/muslimah.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invitation_gallery`
--

CREATE TABLE `invitation_gallery` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_gallery`
--

INSERT INTO `invitation_gallery` (`id`, `invitation_id`, `filename`) VALUES
(20, 10, 'http://localhost/akadin/assets/uploads/2522535.jpg'),
(21, 10, 'http://localhost/akadin/assets/uploads/Sisa-Erupsi-Merapi-Yang-Menjadi-Lava-Tour.jpg'),
(22, 10, 'http://localhost/akadin/assets/uploads/candi-prambanan-pusat-hari-nyepi-di-dunia-JKouHUcDWX.jpg'),
(23, 10, 'http://localhost/akadin/assets/uploads/delokal-bukit-paralayang-jogja.jpg'),
(24, 10, 'http://localhost/akadin/assets/uploads/Harga-Tiket-Masuk-Taman-Sari-Jogja.jpg'),
(25, 10, 'http://localhost/akadin/assets/uploads/20507583_1531555953568044_162724143130407848_o.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `invitation_templates`
--

CREATE TABLE `invitation_templates` (
  `id` int(11) NOT NULL,
  `filename` varchar(55) NOT NULL,
  `ui_desktop` varchar(255) NOT NULL,
  `ui_mobile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_templates`
--

INSERT INTO `invitation_templates` (`id`, `filename`, `ui_desktop`, `ui_mobile`) VALUES
(7, 'a1', 'http://localhost/akadin/assets/uploads/airasia_seat2.jpg', 'http://localhost/akadin/assets/uploads/btn-foodbox2.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `date_created` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `status`, `date_created`) VALUES
(2, 'admin', '12e7263820d64c701ca514da44359f64cc885b116126deb1ff39c93d2c248ab97b348df0d7ef00c10319ae57d2022f81e956681046d5d0ca3c33714df11824d7ngwSy+wDxTg4MuCrgy8Y9RJlp+XoXquhMdEwA+P/kBE=', 1, 1, '1571546864');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitation_contents`
--
ALTER TABLE `invitation_contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitation_gallery`
--
ALTER TABLE `invitation_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitation_templates`
--
ALTER TABLE `invitation_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `invitation_contents`
--
ALTER TABLE `invitation_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `invitation_gallery`
--
ALTER TABLE `invitation_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `invitation_templates`
--
ALTER TABLE `invitation_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
