-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2019 at 04:56 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akadin`
--

-- --------------------------------------------------------

--
-- Table structure for table `invitation_contents`
--

CREATE TABLE `invitation_contents` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `nickname_man` varchar(55) NOT NULL,
  `nickname_woman` varchar(55) NOT NULL,
  `fullname_man` varchar(55) NOT NULL,
  `fullname_woman` varchar(55) NOT NULL,
  `desc_man` varchar(255) NOT NULL,
  `desc_woman` varchar(255) NOT NULL,
  `kota` varchar(55) NOT NULL,
  `withAkad` int(1) NOT NULL,
  `withBasmallah` int(1) NOT NULL,
  `tanggal_akad_nikah` varchar(55) NOT NULL,
  `jam_akad_nikah` varchar(55) NOT NULL,
  `maps_akad_nikah` varchar(255) NOT NULL,
  `lokasi_akad_nikah` varchar(155) NOT NULL,
  `alamat_akad_nikah` varchar(255) NOT NULL,
  `tanggal_resepsi` varchar(55) NOT NULL,
  `jam_resepsi` varchar(55) NOT NULL,
  `maps_resepsi` varchar(255) NOT NULL,
  `lokasi_resepsi` varchar(155) NOT NULL,
  `alamat_resepsi` varchar(255) NOT NULL,
  `foto_sampul` varchar(255) NOT NULL,
  `foto_pria` varchar(255) NOT NULL,
  `foto_wanita` varchar(255) NOT NULL,
  `lagu` varchar(255) NOT NULL,
  `withGaleri` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_contents`
--

INSERT INTO `invitation_contents` (`id`, `invitation_id`, `nickname_man`, `nickname_woman`, `fullname_man`, `fullname_woman`, `desc_man`, `desc_woman`, `kota`, `withAkad`, `withBasmallah`, `tanggal_akad_nikah`, `jam_akad_nikah`, `maps_akad_nikah`, `lokasi_akad_nikah`, `alamat_akad_nikah`, `tanggal_resepsi`, `jam_resepsi`, `maps_resepsi`, `lokasi_resepsi`, `alamat_resepsi`, `foto_sampul`, `foto_pria`, `foto_wanita`, `lagu`, `withGaleri`) VALUES
(10, 10, 'Rahmat', 'Nissa', 'Rahmat Maulana', 'Annisa', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Yogyakarta', 1, 1, '2020-02-20', '08.00 - 09.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', '2020-02-20', '10.00 - 11.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', 'http://localhost/akadin/assets/uploads/845adaf08a9d5ebe8a4bc4110e67def6.jpg', 'http://localhost/akadin/assets/uploads/me_wildan.JPG', 'http://localhost/akadin/assets/uploads/muslimah.png', 'http://localhost/akadin/assets/uploads/06__Seperti_Yang_Kau_Minta_-_MP3_Download,_Play,_Listen_Songs_-_4shared.mp3', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invitation_contents`
--
ALTER TABLE `invitation_contents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invitation_contents`
--
ALTER TABLE `invitation_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
